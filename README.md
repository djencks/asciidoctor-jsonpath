# Asciidoctor jsonpath Extension
:version: 0.1.3-rc.2

`@djencks/asciidoctor-jsonpath` provides an Asciidoctor.js extension to add the results of a query on a json document to an asciidoc document.

WARNING:: This is based on link:https://github.com/dchester/jsonpath[jsonpath] and link:https://github.com/browserify/static-eval[static-eval].
Neither of these should be allowed to execute arbitrary user-supplied code.
For this reason, THIS EXTENSION SHOULD ONLY BE USED ON KNOWN AND TRUSTED CONTENT!.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-jsonpath/-/blob/master/README.adoc.

## Installation

Available through npm as @djencks/asciidoctor-jsonpath.

The project git repository is https://gitlab.com/djencks/asciidoctor-jsonpath

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-jsonpath/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/djencks/asciidoctor-jsonpath/-/blob/master/README.adoc

## Antora Example project

An example project showing some uses of this extension may appear soon at extensions/jsonpath-extension in `https://gitlab.com/djencks/simple-examples`.

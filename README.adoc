= Asciidoctor {extension} Extension
:extension: jsonpath
:extension-version: 0.1.3-rc.2
:antora-version: 3.0.0-rc.2
:source-repository: https://gitlab.com/djencks/asciidoctor-{extension}
:description: This asciidoctor extension provides several ways to use `jsonpath` to query a json-like document and format the results as lists, tables, counts, and repeating blocks.
:rs-url: https://gitlab.com/djencks/asciidoctor-report-support/-/tree/main


WARNING:: This is based on link:https://github.com/dchester/jsonpath[jsonpath] and link:https://github.com/browserify/static-eval[static-eval].
Neither of these should be allowed to execute arbitrary user-supplied code.
For this reason, THIS EXTENSION SHOULD ONLY BE USED ON KNOWN AND TRUSTED CONTENT!.

== Description

{description}
Recognized input formats are json, json5, yaml, and toml.
All formats are imported into a javascript object which can be queried with jsonpath.

Generally the jsonpath query is the first positional parameter.

The project git repository is link:{source-repository}[].

This document contains only a brief description of the processors available.
For detailed descriptions of the output of each processor consult link:{rs-url}[].

Note that all processors for which it makes sense allow you to specify one or more javascript files or npm packages whose exports can be used in the static-eval expressions.

It has one Antora 'contentClassified' event listener, four blockMacro processors, three inlineMacro processors, two block processors, and three include processors.

`indexPage` Antora listener::
This inserts a generated page into the Antora content catalog for each resource selected by the query.
The `query` section must specify the json-ish document to be queried and the jsonpath query, for example:

[source,yaml]
----
    - query:
      version: '1.0',
      component: 'c1',
      module: 'ROOT',
      target: 'example$json/data2.json',
      query: 'nodes$.componentProperties.*'
----
jsonpathTable::
A block macro to be used immediately after a table.
The results of the query are appended as rows to the table.

jsonpathList::
A block macro to be used standalone or immediately after a possibly nested list.
The results of the query are appended as list items in a new or existing unordered list.

jsonpathOrderedList::
A block macro to be used standalone or immediately after a possibly nested list.
The results of the query are appended as list items in a new or existing ordered list.

jsonpathDescriptionList::
A block macro to be used standalone or immediately after a possibly nested list.
The results of the query are appended as list items in a new or existing description list.

jsonpath$::
An include processor that adds the specified expressions evaluated on the results of the query to the document as attributes and header attributes.

jsonpathcount$::
An include processor that adds the counts of the results of the queries to the document as attributes and header attributes.

jsonpathuniquecount$::
An include processor that adds the counts of unique values of the specified expressions evaluated on the results of the queries to the document as attributes and header attributes.

jsonpathCount::
An inline macro that returns the count of the results of the query.

jsonpathUniqueCount::
An inline macro that returns the number of unique values of the results of the query.

jsonpathBlock::
A block that, for each item returned by the query, substitutes the values into the block contents, used as a template.
Each template instance is a separate isolated subdocument.
Therefore it is safe to use the attribute include processors in the template: each template instance will get it's own values of the added attributes.

jsonpathTemplate::
A block that, for each item returned by the query, substitutes the values into the block contents, used as a template.
The results are aggregated into one text string and added to the document with no isolation.
Therefore use of attribute include processors will not work: only the last instance's values will appear.

jsonpathExpression::
Inline and block processors that insert the results of evaluating a single expression.
If the result is a javascript object, it may be written in yaml or json by specifying the format as `outputFormat`.

== Installation

Available through npm as `@djencks/asciidoctor-{extension}`.

=== Installation for Asciidoctor.js

To use in your Asciidoctor documentation project, the simplest way is to have a package.json like this:

[source,json,subs="+attributes"]
----
{
  "description": "---",
  "devDependencies": {
    "@djencks/asciidoctor-{extension}": "^{extension-version}",
    "@asciidoctor/core": "^2.2.0",
    "@asciidoctor/cli": "^2.2.0"
  }
}
----

Other than the code in the tests, I don't know how to use this in standalone asciidoctor.js.

=== Installation for Antora

To use in your Antora documentation project, the simplest way is to have a package.json like this:

[source,json,subs="+attributes"]
----
{
  "description": "---",
  "scripts": {
    "build": "antora antora-playbook.yml --stacktrace --fetch"
  },
  "devDependencies": {
    "@antora/cli": "^{antora-version}",
    "@antora/site-generator-default": "^{antora-version}",
    "@djencks/asciidoctor-{extension}": "^{extension-version}"
  }
}
----

and to include this in your `antora-playbook.yml` playbook:

[source,yml,subs="+attributes"]
----
pipeline:
  extensions:
    - require: '@djencks/asciidoctor-{extension}'
#      trace: true <1>
----
<1> See <<#_logging>>

It is also possible to register this in Antora as an Asciidoctor extension, but this will reduce the functionality of the `requires` feature:

[source,yml,subs="+attributes"]
----
asciidoc:
  extensions:
    - '@djencks/asciidoctor-{extension}'
----

== Configuration

All processors require configuration with a target json document, a query expression, and generally mapping information for the results.

For information on the format of jsonpath query expressions, see link:https://github.com/dchester/jsonpath[jsonpath].

The results of a jsonpath query are in an array, with objects of various complexity.
These objects are generally not flat, but require javascript expressions to extract the data of interest.
For simple expressions, direct use of javascript expressions in static-eval is reasonable.
It is often difficult to escape javascript in AsciiDoc, and static-eval's handling of null and unknown values differs from that of javascript, so for any moderately complicated processing it is advisable to write a helper and reference it with `requires`.

Generally when using a format string it's necessary to escape the opening curly bracket to avoid interpretation as a page attribute use.
For instance, `{backtick}description: ($\\{description}){backtick}`.

WARNING: (Inline `jsonpathCount`, `jsonpathUniqueCount` and `jsonpathExpression` macros.)
Use of the `{star}` character is problematical in inline macros, since pairs get quotes-converted to `<strong>...</strong>` pairs before the macro is recognized.
To avoid this problem, escape all `{star}` as `\*` or define and use a `:star: {star}` attribute.
Any leftover `\*` sequences in macro attributes are substituted back to `{star}`


== Logging configuration

Detailed trace logging may be enabled by setting the pipeline extension config item `trace` to true, or by setting the AsciiDoc attribute `jsonpath-trace` globally or as a page attribute.
Trace logging is logged at `debug` level; you must also set the runtime.log.level key to `debug` to see output.

== Examples

A small example project is in the link:https://gitlab.com/djencks/simple-examples[Simple Examples] project at extensions/jsonpath-extension.

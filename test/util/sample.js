module.exports = function (x) {
  return `argument is ${x}`
}

module.exports.ternary = function (x, y) {
  if (x) return 'x only'
  if (y) return 'y only'
  return 'both x and y'
}
